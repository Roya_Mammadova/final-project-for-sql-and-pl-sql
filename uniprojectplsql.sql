--Student Management
CREATE OR REPLACE PACKAGE uni_management AS
  -- Procedure for Student Registration
  PROCEDURE register_student(
    p_first_name uni_students.first_name%type,
    p_last_name uni_students.last_name%type,
    p_dob uni_students.dob%type,
    p_gender uni_students.gender%type,
    p_email uni_students.email%type,
    p_password uni_students.password%type,
    p_group_id uni_groups.id%type
  );

  -- Procedure for Student Profile Management
  PROCEDURE manage_student_profile(
    p_student_id uni_students.id%type,
    p_first_name uni_students.first_name%type,
    p_last_name uni_students.last_name%type,
    p_dob uni_students.dob%type,
    p_gender uni_students.gender%type,
    p_email uni_students.email%type,
    p_password uni_students.password%type,
    p_group_id uni_groups.id%type
  );

  -- Procedure for Enrollment in Courses
  PROCEDURE enroll_in_course(
    p_student_id uni_students.id%type,
    p_course_id uni_courses.id%type
  );

  -- Procedure to View Grade Cards
  PROCEDURE view_grade_card(
    p_student_id uni_students.id%type
  );

END uni_management;
/

CREATE OR REPLACE PACKAGE BODY uni_management AS
  -- Procedure for Student Registration
  PROCEDURE register_student(
    p_first_name uni_students.first_name%type,
    p_last_name uni_students.last_name%type,
    p_dob uni_students.dob%type,
    p_gender uni_students.gender%type,
    p_email uni_students.email%type,
    p_password uni_students.password%type,
    p_group_id uni_groups.id%type
  ) AS
  v_err varchar2(100);
  v_student_id uni_students.id%type;
BEGIN
  IF p_first_name IS NULL OR p_last_name IS NULL OR p_email IS NULL THEN
    RAISE_APPLICATION_ERROR(-20001, 'Please provide valid values for First Name, Last Name, and Email.');
  END IF;

  INSERT INTO uni_students (first_name, last_name, dob, gender, email, enrollment_date, password, group_id)
  VALUES (p_first_name, p_last_name, p_dob, p_gender, p_email, SYSDATE, p_password, p_group_id);

  -- Fetch the generated ID
  SELECT id INTO v_student_id FROM uni_students WHERE email = p_email;

  -- Log student registration information
  INSERT INTO logs_student_man VALUES(v_student_id, sysdate);
  COMMIT;
EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    v_err := SQLERRM;
    INSERT INTO logs_student_man VALUES (v_err,sysdate);
    log_student_man(v_err);
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20002, 'Email already exists. Please use a different email address.');
  WHEN OTHERS THEN
    v_err := SQLERRM;
   INSERT INTO logs_student_man VALUES (v_err,sysdate);
    log_student_man(v_err);
    ROLLBACK;
    RAISE_APPLICATION_ERROR(-20003, 'Error registering student: ' || SQLERRM);
END register_student;

  -- Procedure for Student Profile Management
 PROCEDURE manage_student_profile(
    p_student_id uni_students.id%TYPE,
    p_first_name uni_students.first_name%TYPE,
    p_last_name uni_students.last_name%TYPE,
    p_dob uni_students.dob%TYPE,
    p_gender uni_students.gender%TYPE,
    p_email uni_students.email%TYPE,
    p_password uni_students.password%TYPE,
    p_group_id uni_groups.id%TYPE
) AS
    v_err varchar2(100);
    v_student_id uni_students.id%type;
BEGIN
    -- Check for null values
    IF p_first_name IS NULL OR p_last_name IS NULL OR p_email IS NULL THEN
        RAISE_APPLICATION_ERROR(-20004, 'Please provide valid values for First Name, Last Name, and Email.');
    END IF;
    -- Fetch the generated ID
      SELECT id INTO v_student_id FROM uni_students WHERE email = p_email;
    BEGIN
        -- Attempt to update the student profile
        UPDATE uni_students
        SET first_name = p_first_name,
            last_name = p_last_name,
            dob = p_dob,
            gender = p_gender,
            email = p_email,
            password = p_password,
            group_id = p_group_id
        WHERE id = p_student_id;
         INSERT INTO logs_student_man VALUES(v_student_id, sysdate);
        COMMIT;
        -- If the update didn't affect any rows, raise the "Student not found" exception
        IF SQL%ROWCOUNT = 0 THEN
            RAISE_APPLICATION_ERROR(-20005, 'Student not found.');
        END IF;

        -- If the update was successful, commit the transaction
        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            -- Handle other exceptions and rollback
            v_err := SQLERRM;
            INSERT INTO logs_student_man VALUES (v_err,sysdate);
            log_student_man(v_err);
            ROLLBACK;
            RAISE_APPLICATION_ERROR(-20006, 'Error managing student profile: ' || SQLERRM);
    END;
END manage_student_profile;


  -- Procedure for Enrollment in Courses
PROCEDURE enroll_in_course(
    p_student_id uni_students.id%type,
    p_course_id uni_courses.id%type
) AS
    v_enrollment_count NUMBER;
    v_err varchar2(100);
BEGIN
    -- Check if the enrollment already exists
    SELECT COUNT(*)
    INTO v_enrollment_count
    FROM uni_course_enrollments
    WHERE student_id = p_student_id AND course_id = p_course_id;

    IF v_enrollment_count > 0 THEN
    INSERT INTO logs_student_man VALUES (v_err,sysdate);
    log_student_man(v_err);
        RAISE_APPLICATION_ERROR(-20007, 'Already enrolled in this course.');
    ELSE
        -- If enrollment does not exist, insert the record
        INSERT INTO uni_course_enrollments (student_id, course_id)
        VALUES (p_student_id, p_course_id);
        INSERT INTO logs_student_man VALUES(p_student_id, sysdate);
        COMMIT;
    END IF;

EXCEPTION
    WHEN OTHERS THEN
    v_err := SQLERRM;
    INSERT INTO logs_student_man VALUES (v_err,sysdate);
    log_student_man(v_err);
    ROLLBACK;
        RAISE_APPLICATION_ERROR(-20008, 'Error enrolling in the course: ' || SQLERRM);
END enroll_in_course;

  -- Procedure to View Grade Cards
PROCEDURE view_grade_card(
    p_student_id uni_students.id%type
) AS
    CURSOR grade_cursor IS
        SELECT c.course_name, e.exam_name, g.grade
        FROM uni_grades g
        JOIN uni_exams e ON g.exam_id = e.id
        JOIN uni_courses c ON e.course_id = c.id
        WHERE g.student_id = p_student_id;
    grade_rec grade_cursor%ROWTYPE;
BEGIN
    DBMS_OUTPUT.PUT_LINE('Grade Card for Student ID ' || p_student_id || ':');
    OPEN grade_cursor;
    
    -- Fetch the first record
    FETCH grade_cursor INTO grade_rec;
    
    -- Check if no data is found
    IF grade_cursor%NOTFOUND THEN
        RAISE_APPLICATION_ERROR(-20009, 'No grade cards found for the student.');
    ELSE
        -- Process the cursor in a loop
        LOOP
            DBMS_OUTPUT.PUT_LINE('Course: ' || grade_rec.course_name);
            DBMS_OUTPUT.PUT_LINE('Exam: ' || grade_rec.exam_name);
            DBMS_OUTPUT.PUT_LINE('Grade: ' || grade_rec.grade);
            
            FETCH grade_cursor INTO grade_rec;
            EXIT WHEN grade_cursor%NOTFOUND;
        END LOOP;
    END IF;

    CLOSE grade_cursor;
EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20010, 'Error viewing grade card: ' || SQLERRM);
END view_grade_card;
END uni_management;
/



-- Trigger to automatically update the enrollment date when a new student is registered
CREATE OR REPLACE TRIGGER student_registration_trigger
BEFORE INSERT ON uni_students
FOR EACH ROW
BEGIN
  :NEW.enrollment_date := SYSDATE;
END;
/
--Registering new student
BEGIN
  uni_management.register_student('Aysel', 'Ahmadova', TO_DATE('2000-01-01', 'YYYY-MM-DD'), 'Gender', 'royamammadova@example.com', 'password123',1);
END;
/

--Managing student profile
BEGIN
  uni_management.manage_student_profile(11,'UpdatedFirstName', 'UpdatedLastName', TO_DATE('1995-05-15', 'YYYY-MM-DD'), 'Male', 'updatedstudent@example.com', 'newpassword123', 2);
END;
/

set serveroutput on;
--Enrolling in courses
BEGIN
  uni_management.enroll_in_course(2, 1);
END;
/

--Viewing grade cards
BEGIN
  uni_management.view_grade_card(2);
END;
/

--Faculty Management
CREATE OR REPLACE PACKAGE faculty_management_pkg AS
  -- Procedure for Faculty Registration
  PROCEDURE faculty_registration(
    p_name uni_faculties.name%TYPE,
    p_email uni_faculties.email%TYPE,
    p_password uni_faculties.password%TYPE
  );

  -- Procedure for Faculty Profile Management
  PROCEDURE profile_management(
    p_faculty_id uni_faculties.id%TYPE,
    p_name uni_faculties.name%TYPE,
    p_email uni_faculties.email%TYPE
  );

  -- Procedure for Assigning Subjects to Faculty
  PROCEDURE assign_subjects(
    p_teacher_id uni_teachers.id%TYPE,
    p_course_id uni_courses.id%TYPE
  );

  -- Procedure for Inputting Exam Grades
  PROCEDURE input_exam_grades(
    p_student_id uni_students.id%TYPE,
    p_exam_id uni_exams.id%TYPE,
    p_grade NUMBER
  );
END faculty_management_pkg;

/
CREATE OR REPLACE PACKAGE BODY faculty_management_pkg AS
  -- Procedure for Faculty Registration
  PROCEDURE faculty_registration(
    p_name uni_faculties.name%TYPE,
    p_email uni_faculties.email%TYPE,
    p_password uni_faculties.password%TYPE
  ) IS
  v_err varchar2(100);
  v_faculty_id uni_faculties.id%type;
  BEGIN
    INSERT INTO uni_faculties(name, email, password)
    VALUES (p_name, p_email, p_password);
    -- Fetch the generated ID
    SELECT id INTO v_faculty_id FROM uni_faculties WHERE email = p_email;
    INSERT INTO logs_student_man VALUES(v_faculty_id, sysdate);
    COMMIT;
    DBMS_OUTPUT.PUT_LINE('Faculty registered successfully.');
  EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      v_err := SQLERRM;
   INSERT INTO logs_student_man VALUES (v_err,sysdate);
    log_student_man(v_err);
    ROLLBACK;
      DBMS_OUTPUT.PUT_LINE('Error: Faculty with the same email already exists.');
    WHEN OTHERS THEN
   INSERT INTO logs_student_man VALUES (v_err,sysdate);
    log_student_man(v_err);
    ROLLBACK;
      DBMS_OUTPUT.PUT_LINE('Error: Faculty registration failed.');
  END faculty_registration;

  -- Procedure for Faculty Profile Management
PROCEDURE profile_management(
    p_faculty_id uni_faculties.id%TYPE,
    p_name uni_faculties.name%TYPE,
    p_email uni_faculties.email%TYPE
) IS
  v_err varchar2(100);
BEGIN
  -- Try to update the faculty profile
  UPDATE uni_faculties
  SET name = p_name, email = p_email
  WHERE id = p_faculty_id;

  -- Check if any rows were updated
  IF SQL%ROWCOUNT = 0 THEN
    v_err := SQLERRM;
    INSERT INTO logs_student_man VALUES (v_err, sysdate);
    log_student_man(v_err);
  ELSE
    -- Rows were updated successfully
    INSERT INTO logs_student_man VALUES(p_faculty_id, sysdate);
    COMMIT;
    dbms_output.put_line('Faculty profile updated successfully.');
  END IF;
  
EXCEPTION
  WHEN OTHERS THEN
    v_err := SQLERRM;
    INSERT INTO logs_student_man VALUES (v_err, sysdate);
    log_student_man(v_err);
    dbms_output.put_line('Error: Profile update failed.');
END profile_management;

  -- Procedure for Assigning Subjects to Faculty
 PROCEDURE assign_subjects(
    p_teacher_id uni_teachers.id%TYPE,
    p_course_id uni_courses.id%TYPE
  ) IS
    l_assignment_count NUMBER;
    v_err varchar2(100);
  BEGIN
    -- Check if the assignment already exists
    SELECT COUNT(*)
    INTO   l_assignment_count
    FROM   uni_teachers
    WHERE  course_id = p_course_id
    AND    id = p_teacher_id;

    IF l_assignment_count = 0 THEN
      -- If assignment doesn't exist, insert it
      INSERT INTO uni_teachers(id, course_id)
      VALUES (p_teacher_id, p_course_id); 
      COMMIT;
      DBMS_OUTPUT.PUT_LINE('Subjects assigned successfully.');
    ELSE
      -- If assignment already exists, raise a custom exception
      v_err := SQLERRM;
      INSERT INTO logs_student_man VALUES (v_err, sysdate);
      log_student_man(v_err);
      ROLLBACK;
      RAISE_APPLICATION_ERROR(-20001, 'Error: Subject already assigned to the faculty.');
    END IF;
    
  EXCEPTION
    WHEN OTHERS THEN
      v_err := SQLERRM;
      INSERT INTO logs_student_man VALUES (v_err, sysdate);
      log_student_man(v_err);
      ROLLBACK; -- Rollback the transaction in case of error
      RAISE_APPLICATION_ERROR(-20002, 'Error: Subject assignment failed.');
  END assign_subjects;


  -- Procedure for Inputting Exam Grades
  PROCEDURE input_exam_grades(
    p_student_id uni_students.id%TYPE,
    p_exam_id uni_exams.id%TYPE,
    p_grade NUMBER
  ) IS
  v_err varchar2(100);
BEGIN
  INSERT INTO uni_grades(student_id, exam_id, grade)
  VALUES (p_student_id, p_exam_id, p_grade);
  INSERT INTO logs_student_man VALUES(p_student_id, sysdate);
  COMMIT;
  IF SQL%ROWCOUNT = 0 THEN
    -- No rows were inserted; this can happen if either student_id or exam_id is invalid.
    v_err := 'Error: Student or exam not found.';
    INSERT INTO logs_student_man VALUES (v_err, sysdate);
    log_student_man(v_err);
    ROLLBACK;
    DBMS_OUTPUT.PUT_LINE('Error: Student or exam not found.');
  ELSE
    INSERT INTO logs_student_man VALUES(p_student_id, sysdate);
    COMMIT;
    DBMS_OUTPUT.PUT_LINE('Exam grade recorded successfully.');
  END IF;

EXCEPTION
  WHEN OTHERS THEN
    v_err := SQLERRM;
    INSERT INTO logs_student_man VALUES (v_err, sysdate);
    log_student_man(v_err);
    ROLLBACK;
    DBMS_OUTPUT.PUT_LINE('Error: Failed to record exam grade.');
END input_exam_grades;

END faculty_management_pkg;
/
set serveroutput on;

-- Execute Faculty Registration
BEGIN
  faculty_management_pkg.faculty_registration(
    'Jane Smith',
    'jane.smith@example.com',
    'securepassword'
  );
END;
/
-- Execute Faculty Profile Management
BEGIN
  faculty_management_pkg.profile_management(
    10, 
    'Updated Faculty Name',
    'updated.email@example.com'
  );
END;
/
-- Execute Assign Subjects to Faculty
BEGIN
  faculty_management_pkg.assign_subjects(
    2, 
    2
  );
END;
/
-- Execute Input Exam Grades
BEGIN
  faculty_management_pkg.input_exam_grades(
    2, 
    2, 
    100
  );
END;
/
--Course Management
CREATE OR REPLACE PACKAGE course_management_pkg AS
  -- Procedure for Adding a New Course
  PROCEDURE add_course(
    p_course_name uni_courses.course_name%TYPE,
    p_course_description uni_courses.description%TYPE
  );

  -- Procedure for Editing an Existing Course
  PROCEDURE edit_course(
    p_course_id uni_courses.id%TYPE,
    p_new_course_name uni_courses.course_name%TYPE,
    p_new_description uni_courses.description%TYPE
  );

  -- Procedure for Deleting a Course
  PROCEDURE delete_course(
    p_course_id uni_courses.id%TYPE
  );

  -- Procedure for Listing Registered Students in a Course
  PROCEDURE list_registered_students(
    p_course_id uni_courses.id%TYPE
  );

END course_management_pkg;
/
CREATE OR REPLACE PACKAGE BODY course_management_pkg AS
  -- Procedure for Adding a New Course
  PROCEDURE add_course(
    p_course_name uni_courses.course_name%TYPE,
    p_course_description uni_courses.description%TYPE
  ) IS
  v_err varchar2(100);
BEGIN
  -- Check for duplicate course name
  SELECT COUNT(*) INTO v_err
  FROM uni_courses
  WHERE course_name = p_course_name;

  IF v_err > 0 THEN
    v_err := 'Error: Course with the same name already exists.';
    INSERT INTO logs_student_man VALUES (v_err, sysdate);
    log_student_man(v_err);
    DBMS_OUTPUT.PUT_LINE(v_err);
  ELSE
      -- Attempt to insert the course
      INSERT INTO uni_courses(course_name, description)
      VALUES (p_course_name, p_course_description);
      INSERT INTO logs_student_man VALUES (p_course_name || ' added successfully.', sysdate);
      COMMIT;
      DBMS_OUTPUT.PUT_LINE('Course added successfully.');
    end if;
    EXCEPTION
      WHEN OTHERS THEN 
      v_err := SQLERRM;
        INSERT INTO logs_student_man VALUES (v_err,sysdate);
        log_student_man(v_err);
        ROLLBACK;
        DBMS_OUTPUT.PUT_LINE(v_err);
        RAISE;
END add_course;
  -- Procedure for Editing an Existing Course
  PROCEDURE edit_course(
    p_course_id uni_courses.id%TYPE,
    p_new_course_name uni_courses.course_name%TYPE,
    p_new_description uni_courses.description%TYPE
  ) IS
  v_err varchar2(100);
  BEGIN
    UPDATE uni_courses
    SET course_name = p_new_course_name, description = p_new_description
    WHERE id = p_course_id;
    INSERT INTO logs_student_man VALUES(p_course_id, sysdate);
    COMMIT;
    DBMS_OUTPUT.PUT_LINE('Course edited successfully.');
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
     v_err := SQLERRM;
   INSERT INTO logs_student_man VALUES (v_err,sysdate);
    log_student_man(v_err);
    ROLLBACK;
      DBMS_OUTPUT.PUT_LINE('Error: Course not found.');
    WHEN OTHERS THEN
    INSERT INTO logs_student_man VALUES (v_err,sysdate);
    log_student_man(v_err);
      ROLLBACK;
      DBMS_OUTPUT.PUT_LINE('Error: Failed to edit the course.');
  END edit_course;

  -- Procedure for Deleting a Course
PROCEDURE delete_course(
    p_course_id uni_courses.id%TYPE
  ) IS
  v_err varchar2(100);
BEGIN
  BEGIN
    -- Attempt to delete the course
    DELETE FROM uni_courses
    WHERE id = p_course_id;
    
    -- Check for rows affected by the delete operation
    IF SQL%ROWCOUNT = 0 THEN
      -- No rows were deleted, indicating that the course was not found
      v_err := 'Error: Course not found.';
      INSERT INTO logs_student_man VALUES (v_err, sysdate);
      log_student_man(v_err);
      ROLLBACK;
      DBMS_OUTPUT.PUT_LINE(v_err);
    ELSE
      -- Course deleted successfully
      INSERT INTO logs_student_man VALUES(p_course_id || ' deleted successfully.', sysdate);
      COMMIT;
      DBMS_OUTPUT.PUT_LINE('Course deleted successfully.');
    END IF;
    
  EXCEPTION
    -- Handle the case of a duplicate ID
    WHEN DUP_VAL_ON_INDEX THEN
      v_err := 'Error: Duplicate course ID found.';
      INSERT INTO logs_student_man VALUES (v_err, sysdate);
      log_student_man(v_err);
      ROLLBACK;
      DBMS_OUTPUT.PUT_LINE(v_err);
      
    -- Handle other exceptions
    WHEN OTHERS THEN
      v_err := SQLERRM;
      INSERT INTO logs_student_man VALUES (v_err, sysdate);
      log_student_man(v_err);
      ROLLBACK;
      DBMS_OUTPUT.PUT_LINE('Error: Failed to delete the course.');
  END;
END delete_course;


  -- Procedure for Listing Registered Students in a Course
  PROCEDURE list_registered_students(
    p_course_id uni_courses.id%TYPE
  ) IS
    v_student_exists BOOLEAN := FALSE;
  BEGIN
    DBMS_OUTPUT.PUT_LINE('List of registered students for the course:');
    
    -- Use a cursor FOR loop to retrieve and display student data
    FOR student_rec IN (
      SELECT id, first_name
      FROM uni_students
      WHERE id IN (
        SELECT student_id
        FROM uni_course_enrollments
        WHERE course_id = p_course_id
      )
    ) LOOP
      DBMS_OUTPUT.PUT_LINE('Student ID: ' || student_rec.id || ', Name: ' || student_rec.first_name);
      v_student_exists := TRUE; -- At least one student is found
    END LOOP;
    
    IF NOT v_student_exists THEN
      DBMS_OUTPUT.PUT_LINE('No students registered for the course.');
    END IF;
    
    EXCEPTION
      WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE('Error: Failed to retrieve the list of registered students.');
  END list_registered_students;


  END course_management_pkg;
/
BEGIN
  course_management_pkg.add_course(
    'Course Name',
    'Course Description'
  );
END;
/

BEGIN
  course_management_pkg.edit_course(
    1,
    'New Course Name',
    'New Course Description'
  );
END;
/
BEGIN
  course_management_pkg.delete_course(
    3
  );
END;
/

BEGIN
  course_management_pkg.list_registered_students(
    2
  );
END;
/
--Exam Management
CREATE OR REPLACE PACKAGE exam_management_pkg AS
  -- Procedure for Creating Exams for Courses
  PROCEDURE create_exam(
    p_course_id uni_courses.id%TYPE,
    p_exam_name uni_exams.exam_name%type,
    p_exam_date uni_exams.exam_date%type
  );

  -- Procedure for Assigning Dates to Exams
  PROCEDURE assign_exam_date(
    p_exam_id uni_exams.id%TYPE,
    p_exam_date uni_exams.exam_date%type
  );

  -- Procedure for Inputting and Displaying Exam Results
  PROCEDURE input_and_display_results(
    p_student_id uni_students.id%TYPE,
    p_exam_id uni_exams.id%TYPE,
    p_grade uni_grades.grade%type
  );

END exam_management_pkg;
/

CREATE OR REPLACE PACKAGE BODY exam_management_pkg AS
  -- Procedure for Creating Exams for Courses
 PROCEDURE create_exam(
    p_course_id uni_courses.id%TYPE,
    p_exam_name uni_exams.exam_name%type,
    p_exam_date uni_exams.exam_date%type
  ) IS
  v_err varchar2(100);
  v_count NUMBER;
BEGIN
  -- Check if a duplicate exam exists for the same course, name, and date
  SELECT COUNT(*)
  INTO v_count
  FROM uni_exams
  WHERE course_id = p_course_id
    AND exam_name = p_exam_name
    AND exam_date = p_exam_date;
    
  IF v_count > 0 THEN
    -- Duplicate exam found
    v_err := 'Error: Duplicate exam found for the same course, name, and date.';
    INSERT INTO logs_student_man VALUES (v_err, sysdate);
    log_student_man(v_err);
    DBMS_OUTPUT.PUT_LINE(v_err);
    else
    -- No duplicate exam found, proceed with exam creation
      -- Attempt to insert the exam
      INSERT INTO uni_exams(course_id, exam_name, exam_date)
      VALUES (p_course_id, p_exam_name, p_exam_date);

      -- Insert a log record for the successful exam creation
      INSERT INTO logs_student_man VALUES (p_exam_name || ' exam created successfully.', sysdate);
      COMMIT;
      DBMS_OUTPUT.PUT_LINE('Exam created successfully.');
    end if;
    EXCEPTION
      -- Handle other exceptions
      WHEN OTHERS THEN
        v_err := SQLERRM;
        INSERT INTO logs_student_man VALUES (v_err, sysdate);
        log_student_man(v_err);
        ROLLBACK;
        DBMS_OUTPUT.PUT_LINE('Error: Failed to create the exam.');
END create_exam;

  -- Procedure for Assigning Dates to Exams
PROCEDURE assign_exam_date(
  p_exam_id uni_exams.id%TYPE,
  p_exam_date uni_exams.exam_date%TYPE
) IS
  v_err varchar2(100);
  l_exam_count NUMBER;
BEGIN
  -- Check if the exam exists
  SELECT COUNT(*)
  INTO l_exam_count
  FROM uni_exams
  WHERE id = p_exam_id;

  IF l_exam_count = 0 THEN
    -- If the exam doesn't exist, raise a custom exception
    v_err := SQLERRM;
   INSERT INTO logs_student_man VALUES (v_err,sysdate);
    log_student_man(v_err);
    RAISE_APPLICATION_ERROR(-20001, 'Error: Exam not found.');
  ELSE
    -- If the exam exists, assign the new date
    UPDATE uni_exams
    SET exam_date = p_exam_date
    WHERE id = p_exam_id;
    INSERT INTO logs_student_man VALUES(p_exam_id, sysdate);
    COMMIT;
    DBMS_OUTPUT.PUT_LINE('Exam date assigned successfully.');
  END IF;
  
EXCEPTION
  WHEN OTHERS THEN
   v_err := SQLERRM;
   INSERT INTO logs_student_man VALUES (v_err,sysdate);
    log_student_man(v_err);
    ROLLBACK;
    -- Check if the custom exception was raised
    IF SQLCODE = -20001 THEN
      DBMS_OUTPUT.PUT_LINE(SQLERRM);
    ELSE
      DBMS_OUTPUT.PUT_LINE('Error: Failed to assign exam date.');
    END IF;
END assign_exam_date;


  -- Procedure for Inputting and Displaying Exam Results
  PROCEDURE input_and_display_results(
    p_student_id uni_students.id%TYPE,
    p_exam_id uni_exams.id%TYPE,
    p_grade uni_grades.grade%type
  ) IS
  v_err varchar2(100);
  BEGIN
    INSERT INTO uni_grades(student_id, exam_id, grade)
    VALUES (p_student_id, p_exam_id, p_grade);
    INSERT INTO logs_student_man VALUES(p_student_id, sysdate);
    COMMIT;
    DBMS_OUTPUT.PUT_LINE('Exam grade recorded successfully.');
    
    -- Display the exam results for the student
    DBMS_OUTPUT.PUT_LINE('Exam Results for Student ID ' || p_student_id || ' in Exam ID ' || p_exam_id || ':');
    
    FOR grade_rec IN (
      SELECT grade
      FROM uni_grades
      WHERE student_id = p_student_id
      AND exam_id = p_exam_id
    ) LOOP
      DBMS_OUTPUT.PUT_LINE('Grade: ' || grade_rec.grade);
    END LOOP;
    
    EXCEPTION
      WHEN OTHERS THEN
       v_err := SQLERRM;
   INSERT INTO logs_student_man VALUES (v_err,sysdate);
    log_student_man(v_err);
    ROLLBACK;
        DBMS_OUTPUT.PUT_LINE('Error: Failed to record or display exam grade.');
  END input_and_display_results;

END exam_management_pkg;
/
BEGIN
  exam_management_pkg.create_exam(
    1, 
    'Midterm Exam',
    TO_DATE('2023-10-15', 'YYYY-MM-DD')
  );
END;
/
BEGIN
  exam_management_pkg.assign_exam_date(
    1, 
    TO_DATE('2023-11-10', 'YYYY-MM-DD')
  );
END;
/
BEGIN
  exam_management_pkg.input_and_display_results(
    4, 
    2, 
    90
  );
END;
/
--Administration
CREATE OR REPLACE PACKAGE admin_management_pkg AS
  -- Procedure for User Authentication
  PROCEDURE authenticate_user(
    p_email IN VARCHAR2,
    p_password IN VARCHAR2,
    p_user_type OUT VARCHAR2
  );

  -- Procedure for Generating Reports
  PROCEDURE generate_report(
    p_report_type IN VARCHAR2
  );

END admin_management_pkg;
/
CREATE OR REPLACE PACKAGE BODY admin_management_pkg AS
  -- Procedure for User Authentication
  PROCEDURE authenticate_user(
    p_email IN VARCHAR2,
    p_password IN VARCHAR2,
    p_user_type OUT VARCHAR2
  ) IS
    v_user_type VARCHAR2(20);
  BEGIN
    -- Check if the user is an Admin
    SELECT 'Faculty' INTO v_user_type
    FROM uni_faculties
    WHERE email = p_email AND password = p_password;

    IF v_user_type IS NULL THEN
      -- Check if the user is a Faculty
      SELECT 'Teacher' INTO v_user_type
      FROM uni_teachers
      WHERE email = p_email AND password = p_password;
      
      IF v_user_type IS NULL THEN
        -- Check if the user is a Student
        SELECT 'Student' INTO v_user_type
        FROM uni_students
        WHERE email = p_email AND password = p_password;
      END IF;
    END IF;

    IF v_user_type IS NOT NULL THEN
      p_user_type := v_user_type;
    ELSE
      p_user_type := 'Invalid';
    END IF;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_user_type := 'Invalid';
    WHEN OTHERS THEN
      p_user_type := 'Error';
  END authenticate_user;

  -- Procedure for Generating Reports
PROCEDURE generate_report(
  p_report_type IN VARCHAR2
) IS
BEGIN
  -- Implement report generation logic based on the report type
  CASE p_report_type
    WHEN 'StudentListPerCourse' THEN
      -- Generate the Student List per Course report
      FOR course_rec IN (SELECT c.course_name, s.first_name, s.last_name
                         FROM uni_courses c
                         JOIN uni_enrollments e ON c.id = e.course_id
                         JOIN uni_students s ON e.student_id = s.id
                         ORDER BY c.course_name, s.last_name, s.first_name) 
      LOOP
        DBMS_OUTPUT.PUT_LINE('Course: ' || course_rec.course_name);
        DBMS_OUTPUT.PUT_LINE('Student: ' || course_rec.first_name || ' ' || course_rec.last_name);
        DBMS_OUTPUT.PUT_LINE('---');
      END LOOP;

    WHEN 'ExamReports' THEN
      -- Generate Exam Reports logic here for a different report type
      FOR exam_rec IN (SELECT e.exam_name, e.exam_date, c.course_name
                      FROM uni_exams e
                      JOIN uni_courses c ON e.course_id = c.id
                      ORDER BY c.course_name, e.exam_date) 
      LOOP
        DBMS_OUTPUT.PUT_LINE('Course: ' || exam_rec.course_name);
        DBMS_OUTPUT.PUT_LINE('Exam Name: ' || exam_rec.exam_name);
        DBMS_OUTPUT.PUT_LINE('Exam Date: ' || TO_CHAR(exam_rec.exam_date, 'DD-MON-YYYY'));
        DBMS_OUTPUT.PUT_LINE('---');
      END LOOP;


    ELSE
      DBMS_OUTPUT.PUT_LINE('Unknown report type: ' || p_report_type);
  END CASE;
END generate_report;

END admin_management_pkg;
/
-- Execute the Reports Generation Procedure for Student List per Course
BEGIN
  admin_management_pkg.generate_report(
    p_report_type => 'StudentListPerCourse' 
  );
END;
/

-- Execute the Reports Generation Procedure for Exam Reports
BEGIN
  admin_management_pkg.generate_report(
    p_report_type => 'ExamReports' 
  );
END;
/

--Function for calculating gpa  from  student id
CREATE OR REPLACE FUNCTION calculate_gpa(p_student_id IN NUMBER) RETURN NUMBER IS
    v_total_grade_points NUMBER := 0;
    v_total_credits NUMBER := 0;
BEGIN
    -- Calculate GPA based on grades and credits for courses
    FOR grade_rec IN (SELECT g.grade, c.credits
                      FROM uni_grades g
                      JOIN uni_courses c ON g.course_id = c.id
                      WHERE g.student_id = p_student_id) 
    LOOP
        v_total_grade_points := v_total_grade_points + (grade_rec.grade * grade_rec.credits);
        v_total_credits := v_total_credits + grade_rec.credits;
    END LOOP;

    -- Avoid division by zero
    IF v_total_credits = 0 THEN
        RETURN NULL;
    END IF;

    -- Calculate GPA (assuming a 4.0 scale)
    RETURN v_total_grade_points / v_total_credits;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN NULL; -- Handle the case when no grades are found for the student
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20011, 'Error calculating GPA: ' || SQLERRM);
END calculate_gpa;

/
-- Example SQL query to calculate GPA for a student with a specific ID
DECLARE
  v_student_id NUMBER := 43;
  v_gpa NUMBER;
BEGIN
  v_gpa := calculate_gpa(v_student_id);
  DBMS_OUTPUT.PUT_LINE('GPA for Student ' || v_student_id || ': ' || v_gpa);
END;
/








