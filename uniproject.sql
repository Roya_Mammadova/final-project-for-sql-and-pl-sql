-- Create the Groups table
CREATE TABLE uni_groups (
    id NUMBER generated always as identity primary key,
    group_name VARCHAR2(50)
);

-- Create the Education_Forms table
CREATE TABLE uni_education_forms (
    id NUMBER generated always as identity primary key,
    form_name VARCHAR2(50)
);

-- Create the Sectors table
CREATE TABLE uni_sectors (
    id NUMBER generated always as identity primary key,
    sector_name VARCHAR2(50)
);

-- Create the Students table
CREATE TABLE uni_students (
    id NUMBER generated always as identity primary key,
    first_name VARCHAR2(50),
    last_name VARCHAR2(50),
    dob DATE,
    gender VARCHAR2(10),
    email VARCHAR2(100) UNIQUE,
    enrollment_date DATE,
    password VARCHAR2(100),
    group_id NUMBER, -- Foreign Key to Groups
    CONSTRAINT fk_student_group FOREIGN KEY (group_id) REFERENCES uni_groups(id)
);

-- Create the Faculty table
CREATE TABLE uni_faculties(
    id NUMBER generated always as identity primary key,
    name VARCHAR2(100),
    email VARCHAR2(100) UNIQUE,
    password VARCHAR2(100)
);

-- Create the Departments table
CREATE TABLE uni_departments (
    id NUMBER generated always as identity primary key,
    department_name VARCHAR2(100),
    faculty_id NUMBER, -- Foreign Key to Faculty
    sector_id NUMBER, -- Foreign Key to Sectors
    CONSTRAINT fk_department_faculty FOREIGN KEY (faculty_id) REFERENCES uni_faculties(id),
    CONSTRAINT fk_department_sector FOREIGN KEY (sector_id) REFERENCES uni_sectors(id)
);

-- Create the Courses table
CREATE TABLE uni_courses (
    id NUMBER generated always as identity primary key,
    course_name VARCHAR2(100),
    department_id NUMBER, -- Foreign Key to Departments
    credits NUMBER(3),
    description VARCHAR2(4000),
    form_id NUMBER, -- Foreign Key to Education_Forms
    CONSTRAINT fk_course_department FOREIGN KEY (department_id) REFERENCES uni_departments(id),
    CONSTRAINT fk_course_form FOREIGN KEY (form_id) REFERENCES uni_education_forms(id)
);

-- Create the Teachers table
CREATE TABLE uni_teachers (
    id NUMBER generated always as identity primary key,
    first_name VARCHAR2(50),
    last_name VARCHAR2(50),
    email VARCHAR2(100) UNIQUE,
    password VARCHAR2(100),
    course_id NUMBER,
    CONSTRAINT fk_teacher_course FOREIGN KEY (course_id) REFERENCES uni_courses(id)
);

-- Create the Enrollment table
CREATE TABLE uni_enrollments(
    id NUMBER generated always as identity primary key,
    student_id NUMBER, -- Foreign Key to Students
    course_id NUMBER, -- Foreign Key to Courses
    CONSTRAINT fk_enrollment_student FOREIGN KEY (student_id) REFERENCES uni_students(id),
    CONSTRAINT fk_enrollment_course FOREIGN KEY (course_id) REFERENCES uni_courses(id)
);

-- Create the Exams table
CREATE TABLE uni_exams (
    id NUMBER generated always as identity primary key,
    exam_name VARCHAR2(100),
    exam_date DATE,
    course_id NUMBER, -- Foreign Key to Courses
    CONSTRAINT fk_exam_course FOREIGN KEY (course_id) REFERENCES uni_courses(id)
);

-- Create the Grades table
CREATE TABLE uni_grades (
    id NUMBER generated always as identity primary key,
    grade NUMBER,
    student_id NUMBER, -- Foreign Key to Students
    exam_id NUMBER, -- Foreign Key to Exams
    CONSTRAINT fk_grade_student FOREIGN KEY (student_id) REFERENCES uni_students(id),
    CONSTRAINT fk_grade_exam FOREIGN KEY (exam_id) REFERENCES uni_exams(id)
);

-- Create a new table for student enrollments in courses
CREATE TABLE uni_course_enrollments (
    enrollment_id NUMBER ,--Foreign key to Enrollments
    student_id NUMBER, -- Foreign Key to Students
    course_id NUMBER,  -- Foreign Key to Courses
    CONSTRAINT fk_course_enrollment_student FOREIGN KEY (student_id) REFERENCES uni_students(id),
    CONSTRAINT fk_course_enrollment_course FOREIGN KEY (course_id) REFERENCES uni_courses(id),
    CONSTRAINT fk_course_enrollment_enrollment FOREIGN KEY (enrollment_id) REFERENCES uni_enrollments(id)
);


-- Create the Teachers-Groups junction table for many-to-many relationship
CREATE TABLE uni_teachers_groups (
    teacher_id NUMBER, -- Foreign Key to Teachers
    group_id NUMBER,   -- Foreign Key to Groups
    CONSTRAINT fk_teachers_groups_teacher FOREIGN KEY (teacher_id) REFERENCES uni_teachers(id),
    CONSTRAINT fk_teachers_groups_group FOREIGN KEY (group_id) REFERENCES uni_groups(id)
);



---Inserting some data
-- Insert data into uni_faculties
-- Insert data into uni_groups
INSERT INTO uni_groups (group_name)
VALUES ('Group A');

INSERT INTO uni_groups (group_name)
VALUES ('Group B');

-- Insert data into uni_education_forms
INSERT INTO uni_education_forms (form_name)
VALUES ('Full-time');

INSERT INTO uni_education_forms (form_name)
VALUES ('Part-time');

-- Insert data into uni_sectors
INSERT INTO uni_sectors (sector_name)
VALUES ('Science');

INSERT INTO uni_sectors (sector_name)
VALUES ('Humanities');

-- Insert data into uni_faculties
INSERT INTO uni_faculties (name, email, password)
VALUES ('Faculty of Science', 'science@example.com', 'faculty_password');

INSERT INTO uni_faculties (name, email, password)
VALUES ('Faculty of Arts', 'arts@example.com', 'faculty_password2');

-- Insert data into uni_departments
INSERT INTO uni_departments (department_name, faculty_id, sector_id)
VALUES ('Physics Department', 1, 1);

INSERT INTO uni_departments (department_name, faculty_id, sector_id)
VALUES ('English Department', 2, 1);

-- Insert data into uni_courses
INSERT INTO uni_courses (course_name, department_id, credits, description, form_id)
VALUES ('Physics 101', 1, 3, 'Introduction to Physics', 1);

INSERT INTO uni_courses (course_name, department_id, credits, description, form_id)
VALUES ('Literature 101', 2, 3, 'Introduction to Literature', 1);

-- Insert data into uni_students
INSERT INTO uni_students (first_name, last_name, dob, gender, email, enrollment_date, password, group_id)
VALUES ('John', 'Doe', TO_DATE('1995-05-15', 'yyyy-mm-dd'), 'Male', 'john@example.com', TO_DATE('2023-09-01', 'yyyy-mm-dd'), 'student_password', 1);

INSERT INTO uni_students (first_name, last_name, dob, gender, email, enrollment_date, password, group_id)
VALUES ('Alice', 'Smith', TO_DATE('1998-02-20', 'yyyy-mm-dd'), 'Female', 'alice@example.com', TO_DATE('2023-09-01', 'yyyy-mm-dd'), 'student_password2', 1);

-- Insert data into uni_teachers
INSERT INTO uni_teachers (first_name, last_name, email, password, course_id)
VALUES ('Professor', 'Smith', 'prof.smith@example.com', 'teacher_password', 1);

INSERT INTO uni_teachers (first_name, last_name, email, password, course_id)
VALUES ('Professor', 'Johnson', 'prof.johnson@example.com', 'teacher_password2', 2);

-- Insert data into uni_enrollments
INSERT INTO uni_enrollments (student_id, course_id,enrollment_date)
VALUES (1, 1,sysdate);

INSERT INTO uni_enrollments (student_id, course_id,enrollment_date)
VALUES (2, 2,sysdate);

-- Insert data into uni_exams
INSERT INTO uni_exams (exam_name, exam_date, course_id)
VALUES ('Midterm Exam', TO_DATE('2023-10-15', 'yyyy-mm-dd'), 1);

INSERT INTO uni_exams (exam_name, exam_date, course_id)
VALUES ('Final Exam', TO_DATE('2023-12-15', 'yyyy-mm-dd'), 2);

-- Insert data into uni_grades
INSERT INTO uni_grades (grade, student_id, exam_id)
VALUES (85, 1, 1);

INSERT INTO uni_grades (grade, student_id, exam_id)
VALUES (90, 2, 2);

-- Insert two rows into uni_teachers_groups 
INSERT INTO uni_teachers_groups (teacher_id, group_id)
VALUES(1, 1);
  
INSERT INTO uni_teachers_groups (teacher_id, group_id)
VALUES(2, 2);

-- Insert two rows into uni_course_enrollments (
INSERT INTO uni_course_enrollments (enrollment_id, student_id, course_id)
VALUES (1, 1, 1);

INSERT INTO uni_course_enrollments (enrollment_id, student_id, course_id)
VALUES(2, 2, 2); 
COMMIT;

--Reporting
--All data
CREATE OR REPLACE VIEW university_data AS
SELECT
    s.first_name AS student_first_name,
    s.last_name AS student_last_name,
    s.dob AS student_date_of_birth,
    s.gender AS student_gender,
    s.email AS student_email,
    s.enrollment_date AS student_enrollment_date,
    t.first_name AS teacher_first_name,
    t.last_name AS teacher_last_name,
    t.email AS teacher_email,
    c.course_name AS course_name,
    d.department_name AS department_name,
    f.name AS faculty_name,
    g.group_name AS group_name,
    ex.exam_name AS exam_name,
    ex.exam_date AS exam_date,
    gr.grade AS exam_grade
FROM
    uni_students s
LEFT JOIN
    uni_teachers t ON s.id = t.course_id
LEFT JOIN
    uni_enrollments e ON s.id = e.student_id
LEFT JOIN
    uni_courses c ON e.course_id = c.id
LEFT JOIN
    uni_departments d ON c.department_id = d.id
LEFT JOIN
    uni_faculties f ON d.faculty_id = f.id
LEFT JOIN
    uni_groups g ON s.group_id = g.id
LEFT JOIN
    uni_exams ex ON c.id = ex.course_id
LEFT JOIN
    uni_grades gr ON s.id = gr.student_id AND ex.id = gr.exam_id;


--Enrollment Statistics by Department:
--This query provides the total number of students enrolled in each department.
CREATE OR REPLACE VIEW enrollment_statistics_by_department AS
SELECT d.department_name, COUNT(e.id) AS total_students_enrolled
FROM uni_departments d
LEFT JOIN uni_courses c ON d.id = c.department_id
LEFT JOIN uni_enrollments e ON c.id = e.course_id
GROUP BY d.department_name;

--List of Courses with Enrollment Details:
--This query displays a list of courses with their names, department, and the number of students enrolled in each course.
CREATE OR REPLACE VIEW course_enrollment_details AS
SELECT c.course_name, d.department_name, COUNT(e.id) AS enrollment_count
FROM uni_courses c
LEFT JOIN uni_departments d ON c.department_id = d.id
LEFT JOIN uni_enrollments e ON c.id = e.course_id
GROUP BY c.course_name, d.department_name;

--Student Grades Overview:
--This query displays the average grade and the highest grade achieved by each student
CREATE OR REPLACE VIEW student_grades_overview AS
SELECT s.first_name, s.last_name, AVG(g.grade) AS average_grade, MAX(g.grade) AS highest_grade
FROM uni_students s
LEFT JOIN uni_grades g ON s.id = g.student_id
GROUP BY s.first_name, s.last_name;


--Students in Each Group:
--This query lists students in each group.
CREATE OR REPLACE VIEW students_in_each_group AS
SELECT g.group_name, s.first_name, s.last_name
FROM uni_groups g
LEFT JOIN uni_students s ON g.id = s.group_id
ORDER BY g.group_name, s.first_name, s.last_name;

--This query gives the average grades of students over time along with the previous and next exam dates.
CREATE OR REPLACE VIEW grades_and_time AS
SELECT
    g.student_id,
    e.exam_date AS current_exam_date,
    AVG(g.grade) OVER (PARTITION BY g.student_id ORDER BY e.exam_date) AS average_grade,
    LAG(e.exam_date) OVER (PARTITION BY g.student_id ORDER BY e.exam_date) AS previous_exam_date,
    LEAD(e.exam_date) OVER (PARTITION BY g.student_id ORDER BY e.exam_date) AS next_exam_date
FROM
    uni_grades g
LEFT JOIN
    uni_exams e ON g.exam_id = e.id
ORDER BY
    g.student_id, e.exam_date;


--This query gives top 10 students that get highest average grades
CREATE OR REPLACE VIEW highest_avg_grades AS
SELECT
    student_id,
    AVG(grade) AS average_grade
FROM
    uni_grades
GROUP BY
    student_id
ORDER BY
    average_grade DESC
FETCH FIRST 10 ROWS ONLY;

--This report shows the enrollment trends for specific courses over a defined period, helping identify which courses are gaining or losing popularity.
CREATE OR REPLACE VIEW enrollment_trend AS
SELECT
    c.course_name AS "Course",
    EXTRACT(YEAR FROM s.enrollment_date) AS "Enrollment Year",
    COUNT(s.id) AS "Total Students Enrolled"
FROM
   uni_students s
LEFT JOIN
    uni_courses c ON s.group_id = c.id
WHERE
    EXTRACT(YEAR FROM s.enrollment_date) BETWEEN EXTRACT(YEAR FROM CURRENT_DATE) - 4 AND EXTRACT(YEAR FROM CURRENT_DATE)
GROUP BY
    c.course_name, EXTRACT(YEAR FROM s.enrollment_date)
ORDER BY
    "Course", "Enrollment Year";






















