--Logging for student management
CREATE TABLE logs_student_man(
    l_id varchar2(100),
    log_date          DATE
);
select * from logs_student_man order by log_date desc;
/
CREATE OR REPLACE PROCEDURE log_student_man (
    p_param varchar2
) IS
    PRAGMA autonomous_transaction;
BEGIN
    INSERT INTO logs_student_man VALUES (p_param,sysdate);
    COMMIT;
END;
/
